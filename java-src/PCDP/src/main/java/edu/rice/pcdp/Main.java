package edu.rice.pcdp;

import edu.rice.pcdp.SieveActor;

public class Main {
    
    public static void main(String[] args) {
        SieveActor sieveActor = new SieveActor();
        long startTime = System.currentTimeMillis();
        int totalPrimes = sieveActor.countPrimes(Integer.parseInt(args[0]));
        System.out.println("total primes: " + totalPrimes);
        long stopTime = System.currentTimeMillis();
        System.out.println("Length of run: " + (stopTime - startTime));
    }

}
